#  Desafio 9

Nesse desafio, vamos utilizar um LAMBDA que será executado a partir de um topico SNS, para sabermos todos os IPS adicionados pela amazon em um periodo de tempo

## Atividade 1
Criar um codigo lambda em python ou node.js para ler os dados de uma url que vira na mensagem do SNS, e extrarir quantos ips foram alterados para cada serviço da AWS em uma região


## Critérios de aceite / sucesso
* Aplicação lambda deve ser executada sempre que algo for publicado no topico SNS `arn:aws:sns:us-east-1:806199016981:AmazonIpSpaceChanged` 
* A Lambda deve ser provisonada utilizando terraform ou ansible, para toda a configuração
* Um readme com a documentação do que foi realizado e justificativa das escolhas.


## Referências
SNS: https://docs.aws.amazon.com/pt_br/sns/?id=docs_gateway

LAMBDA: https://docs.aws.amazon.com/pt_br/lambda/?id=docs_gateway

LAMBDA MUC: https://muc.mandic.com.br/mod/assign/view.php?id=1542
