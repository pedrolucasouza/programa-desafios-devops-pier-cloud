# Programa de aceleração DevOps

​Olá, seja bem vindo aos desafios do programa de aceleração DevOps.

O programa foi criado com o objetivo de alavancar os seus estudos com uma abordagem teórica e prática simultaneamente, proporcionando assim um estudo mais interativo e mais próximo ao dia-a-dia.

O programa de aceleração é composto por vários desafios com níveis de dificuldade progressivos. Cada desafio é um complemento do outro, então lembre-se de seguir a ordem.
​
## Como funciona

​Para a realização dos desafios, você deve realizar um [*fork*](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork) deste projeto em seu GitLab. Para cada desafio, temos um diretório aqui neste repositório contendo as instruções em um arquivo README.md.

No mesmo diretório do desafio, você irá documentar o passo a passo utilizado para a resolução do desafio.

Lembre-se sempre de realizar o *commit* de cada uma de suas suas alterações. Adicione inclusive os arquivos que irá utilizar, sendo o *commit* inicial de cada um ele em seu formato original, caso tenha copiado de algum exemplo da Internet ou vazio. Se tiver copiado de algum site, não se esqueça de referenciar o link neste primeiro commit.

Seguem os links para os desafios:

[Desafio 1](desafio-01/README.md)

[Desafio 2](desafio-02/README.md)

[Desafio 3](desafio-03/README.md)

[Desafio 4](desafio-04/README.md)

[Desafio 5](desafio-05/README.md)

[Desafio 6](desafio-06/README.md)

[Desafio 7](desafio-07/README.md)

[Desafio 8](desafio-08/README.md)

[Desafio 9](desafio-09/README.md)

[Desafio 10](desafio-10/README.md)

